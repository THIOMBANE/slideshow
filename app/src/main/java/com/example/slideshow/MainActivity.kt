package com.example.slideshow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val photos = mutableListOf(R.drawable.chat,R.drawable.panda,R.drawable.pitbull,R.drawable.tortue,R.drawable.zebre)
    var index = 0
    var countDownTimer:CountDownTimer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
            scrollingAutomatic()
    }

    override fun onStop() {
        super.onStop()
            stopSlideshow()
    }

    fun stopSlideshow() {
        countDownTimer?.cancel()
        countDownTimer = null
    }

    fun buttonNext(button: View) {
        stopSlideshow()
        showNextPhoto()
        scrollingAutomatic()
    }

    fun buttonPrevious(button: View) {
        stopSlideshow()
        showPreviousPhoto()
        scrollingAutomatic()
    }

    fun showNextPhoto() {
        index += 1
        if (index >= photos.size ) {
            index = 0
        }
        imageView.setImageResource(photos[index])
    }

    fun showPreviousPhoto() {
        index -= 1
        if (index < 0 ) {
            index = photos.size - 1
        }
        imageView.setImageResource(photos[index])
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("index", index)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        index = savedInstanceState?.getInt("index")?:0
        imageView.setImageResource(photos[index])
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.settings -> displaySettings()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displaySettings() {
        val intent = Intent(this, SeekBarActivity::class.java)
        startActivity(intent)
    }
    fun scrollingAutomatic() {
        val delay = (LocalPreferences(this).slideShowDelay * 1000).toLong()
        countDownTimer = object : CountDownTimer(delay,delay){
            override fun onTick(p0: Long) {   }

            override fun onFinish() {
               showNextPhoto()
                scrollingAutomatic()
            }
        }
        countDownTimer?.start()
    }




}
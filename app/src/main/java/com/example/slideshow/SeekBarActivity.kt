package com.example.slideshow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_seek_bar.*

class SeekBarActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seek_bar)
            seekBar.setOnSeekBarChangeListener(this)

    }

    override fun onStart() {
        super.onStart()
        seekBar.progress =LocalPreferences(this).slideShowDelay
    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                textView2.text ="$p1 s"
                LocalPreferences(this).slideShowDelay = p1
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {

    }

    override fun onStopTrackingTouch(p0: SeekBar?) {

    }
}